pytextfactor: Text Factorisation in Python
==================================
You have a list of textual elements, with a lot of them having the same base. For example:

 - COMBINING OGONEK ABOVE
 - BALINESE MUSICAL SYMBOL COMBINING BENDE
 - BALINESE MUSICAL SYMBOL COMBINING ENDEP
 - BALINESE MUSICAL SYMBOL GONG
 - BALINESE MUSICAL SYMBOL JEGOGAN
 - BALINESE MUSICAL SYMBOL KEMPLI

Any resemblance to real and actual Unicode names is purely factual.

The goal is to be able to display something shorter (in term of characters) like this:
```
COMBINING OGONEK ABOVE
BALINESE MUSICAL SYMBOL:
 - GONG
 - JEGOGAN
 - KEMPLI
 - COMBINING BENDE
 - COMBINING ENDEP
```
The `text_factor` function will output a dictionary where:

  - `Words` is a base class for `Prefix` and `Suffix` with the following members:
    - `text` and `text_length` methods to get full text or text length
    - `parts` containing the splitted text
  - Keys are instances of `Prefix` where `parts` are tuples like `('BALINESE', 'MUSICAL', 'SYMBOL')`
  - Values are lists of `Suffix` where `parts` are list like `['COMBINING', 'ENDEP']` and keep the original string in the `original` member

You do what you want with the dictionary. As an example, here is a simple printer:
```python
def print_factorized(factors):
    indent = ''
    for prefix, suffixes in sorted(factors.items()):
        if len(prefix) > 1:
            print('{prefix}:'.format(prefix=prefix.text()))
            indent = '  - '
        else:
            indent = ''
        for suffix in suffixes:
            print('{indent}{suffix}'.format(indent=indent, suffix=suffix.text()))
```
This is the function used to print the example above.

Requirements
------------
  - [attrs](https://attrs.readthedocs.io/) − Because everyone should use it.