#!/usr/bin/env python3

import attr


class Words:
    def text_length(self):
        return sum(map(len, self.parts))

    def text(self, joiner=' '):
        return joiner.join(self.parts)


@attr.s
class Suffix(Words):
    parts = attr.ib(cmp=False, validator=attr.validators.instance_of(list))
    original = attr.ib()

    def with_prefix(self, prefix):
        return Suffix(list(prefix.parts) + self.parts, self.original)


@attr.s(frozen=True)
class Prefix(Words):
    parts = attr.ib(convert=lambda x: tuple(x))

    def __len__(self):
        return len(self.parts)

    def __getitem__(self, key):
        new_parts = self.parts[key]
        return Prefix(new_parts) if isinstance(key, slice) else new_parts


def text_factor(text_items):
    prefixes = {}

    def is_suffix_in_longer_prefix(prefixes, current_prefix, suffix):
        for prefix, suffixes in prefixes.items():
            if len(suffixes) > 1 \
                    and len(prefix) > len(current_prefix) \
                    and suffix.original in (s.original for s in suffixes):
                return True
        return False

    def try_move_to_smaller_prefix(prefixes, prefix, suffixes, cut_index):
        smaller_prefix = prefix[:cut_index]
        if smaller_prefix in prefixes:
            prefix_length = prefix.text_length()
            stripped_prefix = prefix[cut_index:]
            stripped_prefix_length = stripped_prefix.text_length()
            delta = stripped_prefix_length * len(suffixes) - prefix_length
            if delta < 0:
                # New way is smaller, let's do it
                prefixes[smaller_prefix].extend(
                    [suffix.with_prefix(stripped_prefix) for suffix in suffixes]
                )
                del prefixes[prefix]
                return True
        return False

    # Build naive map
    for text_item in text_items:
        parts = text_item.split()
        for index, part in enumerate(parts):
            prefix = Prefix(parts[:index])
            suffix = Suffix(parts[index:], text_item)
            prefixes.setdefault(prefix, list()).append(suffix)

    # First cleaning
    for prefix, suffixes in list(prefixes.items()):
        if len(prefix) > 0 and len(suffixes) == 1:
            # Only match with this prefix, useless
            del prefixes[prefix]
        else:
            for suffix in list(suffixes):
                if is_suffix_in_longer_prefix(prefixes, prefix, suffix):
                    # We have a better alternative elswhere
                    suffixes.remove(suffix)
            if len(suffixes) == 0:
                # In case we have removed everything in the previous loop
                del prefixes[prefix]

    # Last try. We were optimistic at first that a longer prefix
    # would result in better factorisation. However, if we have a smaller
    # matching prefix with more elements, it can be shorter to display those
    # longer prefix entries along with the smaller group.
    for prefix, suffixes in list(prefixes.items()):
        for i in range(len(prefix) - 1, 0, -1):
            moved = try_move_to_smaller_prefix(prefixes, prefix, suffixes, i)
            if moved:
                break

    return prefixes


def iter_sorted_prefixes(prefixes):
    sorted_items = list(prefixes.keys())
    empty_prefix = Prefix(())
    # Remove empty prefix
    try:
        sorted_items.remove(empty_prefix)
    except:
        pass
    sorted_items.extend(prefixes.get(empty_prefix, []))
    sorted_items.sort(key=lambda x: tuple(x.parts))
    for item in sorted_items:
        if isinstance(item, Prefix):
            yield item, prefixes[item]
        elif isinstance(item, Suffix):
            yield empty_prefix, [item]
        else:
            yield None, None
