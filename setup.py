from distutils.core import setup
setup(
  name = 'text_factor',
  packages = ['text_factor'],
  version = '1.0',
  description = 'Try to factor text items',
  author = 'Adrien CLERC',
  author_email = 'bugs-python@antipoul.fr',
  url = 'https://framagit.org/Glandos/pytextfactor',
  requires = [
      'attrs'
  ],
  download_url = 'https://framagit.org/Glandos/pytextfactor/repository/archive.tar.bz2?ref=master',
  keywords = ['text', 'factorization'],
  classifiers = [],
)